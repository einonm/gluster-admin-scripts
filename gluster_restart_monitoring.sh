#! /bin/sh
#
# Restart Web Administration monitoring system
#
# Copyright (C) 2020 Ellis Pires <pirese@cardiff.ac.uk>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

NODEFILE=gl_nodelist
ARBSFILE=gl_arbiterlist

usage()
{
cat << EOF
Usage: `basename $0`
This program restarts the Red Hat Web Administration monitoring system by
shutting down all associated processes and restarting them in the correct
order. The program requires the list of gluster data nodes (gl_nodelist)
as well as a separate list containing the arbiter node (gl_arbiterlist).

Mandatory arguments to long options are mandatory for short options too.
  -n, --nodelist=FILE   nodelist file, alternate to default ~/$NODEFILE
                        or \$PWD/$NODEFILE
  -a, --arbiterlist=FILE arbiterlist file, alternate to default ~/$ARBSFILE
                        or \$PWD/$ARBSFILE
  -h, --help            display this help and exit
EOF
}

if [ -f ~/$NODEFILE ]; then
    NODES=`cat ~/$NODEFILE`
elif [ -f $NODEFILE ]; then
    NODES=`cat $NODEFILE`
fi

if [ -f ~/$ARBSFILE ]; then
    ARBS=`cat ~/$ARBSFILE`
elif [ -f $ARBSFILE ]; then
    ARBS=`cat $ARBSFILE`
fi

PARSEOPTS=`getopt -o hn:a: --long help,nodelist:,arbiterlist: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -n | --nodelist )
            NODES=`cat $2`
            shift 2
            ;;
        -a | --arbiterlist )
            ARBS=`cat $2`
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [ ! -v NODES ];then
    echo "No nodelist found in ~/$NODEFILE, \$PWD/$NODEFILE or in nodelist parameter."
    echo
    usage;
    exit 1
fi

if [ ! -v ARBS ];then
    echo "No arbiterlist found in ~/$ARBSFILE, \$PWD/$ARBSFILE or in arbiterlist parameter."
    echo
    usage;
    exit 1
fi

clear

if [ ! -z ARBS ];then
    NODES=$NODES" "$ARBS
fi

# First, stop monitoring services on data nodes and arbiter nodes
for NODE in ${NODES[@]}
do
    echo Stopping monitoring services on client node $NODE:
    ssh root@$NODE \
        'systemctl stop tendrl-node-agent tendrl-gluster-integration collectd'
done

# Commands to stop and restart monitoring services on the monitoring host
COMMANDS=$(cat << EOF
echo 'Stopping monitoring services on monitoring host:';
systemctl stop carbon-cache;
systemctl stop carbon-aggregator;
systemctl stop carbon-relay;
systemctl stop grafana-server;
systemctl stop tendrl-node-agent;
systemctl stop tendrl-monitoring-integration;
systemctl stop tendrl-api;
systemctl stop tendrl-notifier;
systemctl stop etcd;
systemctl stop collectd;
systemctl stop prometheus;
systemctl stop alertmanager;
systemctl stop node_exporter;
systemctl stop httpd;
systemctl stop firewalld;
systemctl stop fail2ban;
sleep 10;
rm -rf /usr/storage/whisper;
echo 'Starting monitoring services on monitoring host:';
systemctl start firewalld;
systemctl start fail2ban;
systemctl start httpd;
systemctl start prometheus;
systemctl start alertmanager;
systemctl start node_exporter;
systemctl start carbon-cache;
systemctl start carbon-aggregator;
systemctl start carbon-relay;
systemctl start grafana-server;
systemctl start etcd;
systemctl start collectd;
systemctl start tendrl-node-agent;
systemctl start tendrl-monitoring-integration;
systemctl start tendrl-api;
systemctl start tendrl-notifier;
EOF
)

# Run the commands on the monitoring host
echo Restarting monitoring services on monitoring host:
ssh root@glustermonitor.cf.ac.uk ${COMMANDS}

# Start the monitoring services on client nodes (including callosum)
for NODE in ${NODES[@]}
do
    echo Starting monitoring services on client node $NODE:
    ssh root@$NODE \
        'systemctl start tendrl-node-agent tendrl-gluster-integration collectd'
done

