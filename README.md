# Gluster Admin Scripts

A set of scripts used for general gluster admin tasks.


## Installation and usage

The scripts have been tested on Fedora Linux (at least up to Fedora 36), and all shell scripts can be run directly from the command line.

All scripts use a local file 'gl_nodelist', listing the hostnames of the nodes to be affected by the script.

* gluster_dmesg_xfs_check.sh - checks all nodes' dmesg output for XFS related issues
* gluster_estimate_arbiter_brick_size.sh - estimates the minimum arbiter brick size for utilised data bricks on each node
* gluster_file_brick_check.sh - interrogates the state of an unhealed file or directory, running ls/stat/getfattr on all instnaces of the file on all bricks
* gluster_fstrim_bricks.sh - runs fstrim on all bricks in the cluster. This brings the amount of allocated space in line with the used space
* gluster_gather_sosreports.sh - runs sosreport on all nodes, collecting the output in local directories
* gluster_restart_ctdb_service.sh - brings up the CTDB and supporting services in a working state, tackling several gotchas along the way
* gluster_restart_monitoring.sh - restarts the Red Hat Web Administration monitoring system

## Contributing
Contributing issues, feature proposals or code is actively welcomed - please see the [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Code of Conduct
We want to create a welcoming environment for everyone who is interested in contributing. Please see the [Contributor Covenant Code of Conduct](CODE_OF_CONDUCT.md) file to learn more about our commitment to an open and welcoming environment.

## Copyright and Licence Information

See the [LICENSE file](LICENSE).
 gluster_switch_monitoring.sh - Swaps between Nagios/RHSC and Web Admin/Grafana monitoring systems
