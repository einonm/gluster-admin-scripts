#! /bin/sh
#
# Collate all dmesgs from a node list
#
# Copyright (C) 2019  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

NODEFILE=gl_nodelist

usage()
{
cat << EOF
Usage: `basename $0` [OPTIONS]...
Searches dmesg on a list of servers and returns instances of XFS messages.
Useful for monitoring gluster bricks for issues.

Mandatory arguments to long options are mandatory for short options too.
  -n, --nodelist=FILE   nodelist file, alternate to default ~/$NODEFILE
                        or \$PWD/$NODEFILE

  -h, --help            display this help and exit
EOF
}

if [ -f ~/$NODEFILE ]; then
    NODES=`cat ~/$NODEFILE`
elif [ -f $NODEFILE ]; then
    NODES=`cat $NODEFILE`
fi

PARSEOPTS=`getopt -o hn: --long help,nodelist: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -n | --nodelist )
            NODES=`cat $2`
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [ ! -v NODES ];then
    echo "No nodelist found in ~/$NODEFILE, \$PWD/$NODEFILE or in nodelist parameter."
    echo
    usage;
    exit 1
fi

for NODE in ${NODES[@]}
do
    echo " ************ running on $NODE :"
    ssh root@$NODE 'dmesg | grep -i xfs'
done

